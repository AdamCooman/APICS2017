set PATH=%PATH%;C:\Program Files (x86)\Inkscape;C:\Program Files\Inkscape

forfiles /m *.pdf /c "cmd /c inkscape.exe --without-gui --file=@fname.pdf --export-png=@fname.png --export-dpi=500"

forfiles /m *.svg /c "cmd /c inkscape.exe --without-gui --file=@fname.svg --export-png=@fname.png --export-dpi=500"

